# -*- coding: utf-8 -*-

# Standard library imports
from __future__ import unicode_literals
from django.conf.urls import patterns, url


urlpatterns = patterns('',
                       url(r'^index/$', 'dashboard.views.index', name="index"),
                       url('^message/(?P<message_pk>\d+)/$', 'dashboard.views.message', name="getMessage"),
                       url(r'^remove/(?P<friend_pk>[\d]+)/$', 'dashboard.views.remove_friend', name="remove"),
                       )
