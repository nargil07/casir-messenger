# -*- coding: utf-8 -*-

# Standard library imports
from __future__ import unicode_literals

from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.http import JsonResponse
from django.shortcuts import render
from django.shortcuts import redirect
from django.shortcuts import get_object_or_404
from django.contrib.auth.decorators import login_required
from django.contrib import messages

from members.models import Member
from members.forms import AddFriendForm
from members.forms import PendingRequestForm
from messaging.forms import SendMessageForm
from messaging.models import Message


@login_required
def index(request):
    current_member = request.user.member
    message_form = SendMessageForm(member=current_member)
    add_friend_form = AddFriendForm(member=current_member)
    pending_request_form = PendingRequestForm(member=current_member)
    messages_received = Message.objects.filter(recipient=current_member)
    detail_user = ""
    if request.method == "POST":
        if "add_friend_request" in request.POST:
            add_friend_form = AddFriendForm(member=current_member, data=request.POST)
            pending_request_form = PendingRequestForm(member=current_member)
            if add_friend_form.is_valid():
                for member in add_friend_form.cleaned_data["members"]:
                    member.friend_requests.add(current_member)
                add_friend_form = AddFriendForm(member=current_member)

                messages.success(request, "Demandes d'ajout effectuées avec succès")
        elif "send_message" in request.POST:

            message_form = SendMessageForm(member=current_member, data=request.POST, files=request.FILES)
            if message_form.is_valid():
                Message(author=current_member,
                        recipient=message_form.cleaned_data['recipient'],
                        content=message_form.cleaned_data['content'],
                        duration=message_form.cleaned_data['duration'],
                        picture=message_form.cleaned_data['picture']) \
                    .save()
                message_form = SendMessageForm(member=current_member)

        else:
            pending_request_form = PendingRequestForm(member=current_member, data=request.POST)
            add_friend_form = AddFriendForm(member=current_member)
            if pending_request_form.is_valid():
                if "refuse_pending_request" in request.POST:
                    for member in pending_request_form.cleaned_data["members"]:
                        current_member.friend_requests.remove(member)
                    messages.success(request, "Demandes en attente refusées avec succès")
                elif "accept_pending_request" in request.POST:
                    for member in pending_request_form.cleaned_data["members"]:
                        current_member.friend_requests.remove(member)
                        current_member.friends.add(member)
                    messages.success(request, "Demandes en attente acceptées avec succès")
            else:
                messages.warning(request, "Demandes en attente : opération annulée")
    elif request.method == "GET":
        add_friend_form = AddFriendForm(member=current_member)
        pending_request_form = PendingRequestForm(member=current_member)
        if "detail_user" in request.GET:
            detail_user = request.GET.get("detail_user")
            if detail_user:
                messages_received = messages_received.filter(author__user__username=detail_user)
        if "numero_page" in request.GET:
            numero_page = request.GET.get("numero_page")

    paginator = Paginator(messages_received, 1)

    list_message = []
    try:
        list_message = paginator.page(numero_page)
    except PageNotAnInteger:
        list_message = paginator.page(1)
    except EmptyPage:
        list_message = paginator.page(paginator.num_pages)
    except UnboundLocalError:
        list_message = paginator.page(1)
    friends = current_member.friends.all()
    friend_requests = current_member.friend_requests.all()
    context = {'friends': friends,
               'friend_requests': friend_requests,
               'add_friend_form': add_friend_form,
               'pending_request_form': pending_request_form,
               'message_form': message_form,
               'messages_received': list_message.object_list,
               'current_page': list_message,
               'detail_user': detail_user,
               'paginator': paginator}

    return render(request, "dashboard/index.html", context)


@login_required
def remove_friend(request, friend_pk):
    current_member = request.user.member
    removed_friend = get_object_or_404(Member, pk=friend_pk)
    current_member.friends.remove(removed_friend)
    messages.success(request, "Suppression effectuée avec succès")
    return redirect("dashboard:index")


@login_required
def message(request, message_pk):
    message = get_object_or_404(Message, pk=message_pk)
    if message.is_read:
        return False

    message.is_read = True
    message.save()
    url_picture = ""
    if message.picture == "":
        url_picture = "/media/no_image.png"
    else:
        url_picture = message.picture.url
    data = {
        'picture': url_picture,
        'content': message.content,
        'duration': message.duration,
        'author': message.author.user.username
    }
    return JsonResponse(data)
